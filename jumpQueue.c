#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <unistd.h>

int pass = 0;
int fail = 0;

#define MAX_INPUT_LENGTH 1000
int nRead = 0;
int iStart = 0;
char                inbuf           [4 * MAX_INPUT_LENGTH];

int isCR(char c)  { if (c == '.' || c == '^' ) return 1; return 0; }
//int isCR(char c)  { if (c == '\r' || c == '\n' ) return 1; return 0; }

/***
 *  The '$' command allows input of a command that 'jumps the queue'.
 *
 *  When the driver encounters '$ foo' (looking from the end of the
 *  input command queue) it moves $ foo to the front.
 *
 *  This is useful in many situations, and it gives the player more
 *  control in combat, and also allows fleeing instead of getting
 *  stuck in casting spam.
 *
 *  Some technical notes:
 *
 *  I will edit this all down to something saner (70 chars / line etc),
 *  but this format makes the code easier to showcase.
 *
 *  In the tests, \n and \r have been substituted with ^ and .  just
 *  to make it more readable.
 *
 */

void inputQueueJump()
{
  if ( nRead > 0 ) 
  {
    for (int j = nRead-1;j >= 0; j--)
      {
          int b = iStart+j;
          int e = b;

          if ( inbuf[e] != '$' )                     continue;    // not our target, keep going
          if ( !e )                { inbuf[e] = ' '; continue; }  // we're at the start already, so ignore, but remove the $
          if ( !isCR(inbuf[e-1]) )                   continue;    // ... but not if $ is not the start of a command
          while ( !isCR(inbuf[e]) ) e++;                          // count the letters of the command
          while ( isCR(inbuf[e]) )  e++;                          // now add the CR's

          // at this point we have our $ cmd + CR from iStart to e
          //
          // --------L-------;cmd;------ R-------
          //                 ^
          //               begin (b)
          //
          // turning into:
          //                 b
          // cmd;----L-------;---;------ R-------
          //    ^                ^
          //  cmdLen (c)        end (e)
          //
          // copy the cmd into tmp
          // shift L for c letters backwards
          // copy the cmd at the start of the buffer

          int c = e - b;
          char tmp[c];
          int k;

          for ( k = 0; k < c; k++ ) {       // copy the cmd into tmp[]
            tmp[k] = inbuf[k+b];
            inbuf[k+b] = 0;                 // delete it from inbuf
          }
          for ( k = b+c-1; k > c-1; k-- )  // shift L for c letters backwards
            inbuf[k] = inbuf[k-c];
          for ( k = 0; k < c; k++ )        // copy the cmd at the start of the buffer
            inbuf[k] = tmp[k];
          inbuf[0] = ' ';                  // swap the $ marker for a space
          return;
      }
  }
}

int testItem(int testNr, char* playerInput, char* expect) {

  sprintf(inbuf, "%s", playerInput);
  nRead = strlen(inbuf);

  inputQueueJump();

  if ( !strcmp(inbuf, expect) )  {
    printf("PASS: %2d\n", testNr);
    pass++;
    return 1;
  }
  printf("FAIL: ");
  fail++;
  printf("Test %2d:\n"
         " Input:    >%s<\n"
         " Expect:   >%s<\n"
         " Result:   >%s<\n",
         testNr, playerInput, expect, inbuf);
  return 0;
}

void testRun()
{
  goto nextTest;
 nextTest:
  testItem( 1, "abc^.123^.456^.789^.",    "abc^.123^.456^.789^."     );
  testItem( 2, "$abc^.123^.456^.789^.",   " abc^.123^.456^.789^."    );
  testItem( 3, "$ abc^.123^.456^.789^.",  "  abc^.123^.456^.789^."   );
  testItem( 4, "abcdef^.$123^.456^.789^."," 123^.abcdef^.456^.789^." );
  testItem( 5, "aaa^.$ bbb^.ccc.ddd^.",   "  bbb^.aaa^.ccc.ddd^."    );
  testItem( 6, "abc^.123^.456^.$789^.",   " 789^.abc^.123^.456^."    );
  testItem( 7, "abc^.123^.456^.$ 789^.",  "  789^.abc^.123^.456^."   );
  testItem( 8, "abc^.123^.456^$ 789^.",   "  789^.abc^.123^.456^"    );
  testItem( 9, "a$c^.123^.456^.789^.",    "a$c^.123^.456^.789^."     );
  testItem(10, "$ ab^12$^.456^.789^.",    "  ab^12$^.456^.789^."     );
  testItem(11, "$abc^.12$^.456^.789^.",   " abc^.12$^.456^.789^."    );
  testItem(12, "$.",                      " ."                       );
  testItem(13, "$^",                      " ^"                       );
  testItem(14, "$^.",                     " ^."                      );
  testItem(15, "$.^",                     " .^"                      );
  testItem(16, "",                        ""                         );
  testItem(17, "$ ^.",                    "  ^."                     );
  testItem(18, "$aaa^.",                  " aaa^."                   );
  testItem(19, "$s^.",                    " s^."                     );
  testItem(20, "$ s^.",                   "  s^."                    );

  printf("\n%d test results:  PASS:  %d  FAIL %d\n", pass+fail, pass, fail);
  return;
}

int main()//int argc, char *argv[])
{
  testRun();
  return (EXIT_SUCCESS);
}

//  gcc -ggdb jumpQueue.c;./a.out
