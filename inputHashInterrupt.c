#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <unistd.h>

int pass = 0;
int fail = 0;

#define MAX_INPUT_LENGTH 1000

int nRead = 0;
int iStart = 0;

char inbuf [4 * MAX_INPUT_LENGTH];

int isCR(char c)  { if (c == '\r' || c == '\n' ) return 1; return 0; }
void inputQueueHash();

int testItem(int testNr, char* input, char* expect) {

  sprintf(inbuf, "%s", input);
  nRead = strlen(inbuf);

  inputQueueHash();

  if ( !strcmp(inbuf, expect) )  {
    pass++;
    return 1;
  }
  printf("FAIL: ");
  fail++;
  printf(" %d expected: >%s<  got >%s<\n", testNr, expect, inbuf);
  return 0;
}

void inputQueueHash()
{
  if ( nRead > 0 )
    {
      for (int j = nRead; j > -1 ; j--)
      {
        if ( inbuf[j] != '#' )        continue; // keep looking 
        if ( !isCR(inbuf[j+1]) )      continue; // next char is not a \n or \r  
        if ( j && !isCR(inbuf[j-1]) ) continue; // # in middle of text, ignore 
        int cr = 1;
        if ( isCR(inbuf[j+2]) ) { // increment the CR count if there's 2 cr's
          cr++; inbuf[cr] = ' ';  // then delete the hanging char
        }
        // overwrite the queue with everything that comes after the                                                                                             
        // # and finish with a '\0'.  If there is nothing after the                                                                                             
        // # make the first character an \0.     
        for ( int k = 0; ( inbuf[k] = inbuf[++j+cr]) != '\0'; k++ );
        break;
      }
    }
}

void runTests()
{
  testItem( 1, "sit\r\neat steak\r\nburp\r\nrest\r\n",   "sit\r\neat steak\r\nburp\r\nrest\r\n" );
  testItem( 2, "sit\r\neat steak\r\n#\r\nrest\r\n",      "rest\r\n" );
  testItem( 3, "sit\r\neat steak\r\n#\n\rrest\r\n",      "rest\r\n" );
  testItem( 4, "sit\r\neat steak\r\n#\nrest\r\n",        "rest\r\n" );
  testItem( 5, "sit\r\neat steak\r\n#\n\rrest\r\n#\n",   "" );
  testItem( 6, "#it\r\neat # steak\r\nbu#rp\r\n###\r\n", "#it\r\neat # steak\r\nbu#rp\r\n###\r\n" );
  testItem( 7, "#",                             "#" );
  testItem( 8, "#\n",                           "" );
  testItem( 9, "#\r",                           "" );
  testItem(10, "#\r\n",                         "" );
  testItem(11, "#\n\r",                         "" );
  testItem(12, "",                              "" );

  printf("\n%d test results:  PASS:  %d  FAIL %d\n", pass+fail, pass, fail);
}

int main()
{
  runTests();
  return (EXIT_SUCCESS);
}

// rm a.out;gcc -ggdb inputHashInterrupt.c;./a.out
